﻿#include <iostream>

using namespace std;

int main()
{
	//Variablen Initialisierung
	unsigned int iHoehe = 0;    //Variable f�r die H�he
	unsigned int iBreite = 1;    //Variable f�r die Breite des Baums
	unsigned int iArt = 0;        //Variable f�r die Speicherung der Variante des Weihnachtsbaumes
	unsigned int iCount = 0;    //Zum Gegenzaehlen 
	unsigned int iAbstand = 0;  //Abstandsvariable
	unsigned int iSpeicher = 0;    //Speichert die H�he (f+r Fall 4)
	char cTaste = 'j';            //Eingabe f�r noch einen Baum 
								//Variablen Initialisierung Ende

								//Ausgabe Einf�hrung in Programm
	cout << "\t**Herzlich Willkommen beim Cheap-Christmas-Tree-Generator\n\n";
	cout << "\t\t\t\t**\n";
	cout << "\t\t\t\tHo! Ho! Ho!\n";
	cout << "\t\t\t\t***\n\n";
	while (cTaste == 'j')
	{
		cout << "Variante des gewuenschten Baums eingeben <1-4>: ";
		cin >> iArt;
		cout << "\n\nHoehe des gewuenschten Baums eingeben <5-40>: ";
		cin >> iHoehe;
		cout << "\n";
		if (iHoehe > 40 || iHoehe < 5)
		{
			cout << "Der Weihnachtself sagt:\nBaueme dieser Hoehe haben wir leider nicht im Angebot. \nBitte wenden Sie sich an die Konkurrenz.\n";
		}
		else
		{
			switch (iArt)
			{
			case 1:
				while (iHoehe > 0)                    //Schleife wird solange durchlaufen, bis Hoehe komplett abgearbeitet
				{
					cout << "X\n";
					iHoehe = iHoehe - 1;
				}
				break;
			}
		}
		cout << "\nMoechten Sie noch einen Baum? <j/n>: ";
		cin >> cTaste;
		cout << "\n";
	}//while (cTaste = 'j');
	getchar();
	getchar();
	return 0;
}
